# Introduction

J’ai effectué mon stage chez Orange, à Blagnac, à côté de Toulouse.

En discutant avec mon oncle de mon stage de troisième, il m’a proposé de venir l’effectuer dans l’entreprise où il travaille. Il m’a expliqué que son entreprise avait l’habitude de prendre des stagiaires. A ce moment-là, on était en décembre et je n’avais toujours pas trouvé de stage. J’avais déjà demandé à d’autres entreprises de programmation de jeux-vidéo sur Lyon, mais ils n’ont pas pu m’accepter, pour des raisons de clauses de confidentialité.

Les activités qu’il proposait avaient l’air intéressantes, et le domaine était similaire à ce que je voulais initialement faire.

Pour obtenir mon stage, j’ai envoyé une lettre de motivation à mon oncle, qui est un employé de cette entreprise, pour qu’il puisse la faire passer au service des ressources humaines de celle-ci. 

