# Présentation de l'entreprise

## Entreprise Orange

Les principales activités de l'entreprise sont les **télécommunications**, le cinéma, la télévison et la musique.

L’entreprise a été fondée en 1988 sous le nom de France-Télécom. En 2013, elle change de nom et devient Orange.

Elle emploie plus de **150 000 salariés** dans le monde.

Orange compte **53 millions de clients 4G** et **26,5 millions de foyers raccordables à la fibre**. C’est aussi une banque digitale (Orange Bank).

Comme Amazon ou Google, Orange propose un assistant à intelligence artificielle du nom de **Djingo**.




## Direction TGI/OLS

L’entreprise Orange contient plusieurs sous-entreprises. J’ai effectué mon stage dans celle de **TGI/OLS**

La mission de **TGI** (Technology and Global Innovation) est de préparer le futur d’Orange en travaillant sur la 5G, la virtualisation, l’intelligence artificielle, les services de communications… Pour ça, elle emploie en des chercheurs, des ingénieurs, des sociologues, des graphistes…

Elle compte près de 6 000 salariés dans le monde.


La mission d’**OLS** (Orange Labs Services) est de développer les systèmes d’information et les services. 

Elle compte 18 sites en France et plusieurs à l’étranger comme en Inde, en Roumanie, en Chine, en Tunisie et en Egypte. Elle emploie 3 100 personnes.

