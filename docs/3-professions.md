# Présentation des professions observées

## Dévelopeur

Le métier de développeur se fait à plusieurs, les développeurs s’entraident quand l’un d’entre eux ne sait pas, ils se corrigent quand l’un d’entre eux fait une erreur, peuvent modifier le travail des autres pour l’améliorer.

Le métier se fait avec un ordinateur, un logiciel de programmation et avec, si besoin, un téléphone portable pour tester l’application mobile développée.

Il évolue tout le temps car des nouvelles technologies apparaissent, d’autres ne sont plus utilisées. Les développeurs doivent donc savoir s’adapter, apprendre de nouveaux langages de programmation, etc. C’est un travail qui varie beaucoup.

Il faut aussi passer des journées entières devant un écran, assis, ce qui peut être fatigant pour certaines personnes.

Il faut savoir coder.

Il faut connaître un peu l’anglais car des fois, des textes, des consignes et des logiciels de programmation peuvent être en anglais.

Pour être développeur, il faut avoir fait des études dans une école d’ingénieur spécialisée, bac+2 ou +5.

C’est un métier très recherché donc un développeur a très peu de chances de se retrouver au chômage.

Le télétravail est possible. Le salaire peut varier entre 2 000 et 3 000 euros par mois
