# Conclusion

J’ai trouvé ce stage très intéressant. Dans l’ensemble, ça respectait l’idée que je me faisais d’une entreprise. Celle-ci avait une ambiance assez décontractée, les employés étaient très amicaux, souriants, drôles. Des fois, en arrivant le matin, nous allions à la cafétéria avec les collègues de mon oncle. 

Ces métiers correspondaient assez bien à l’image que je m’en faisais. J’ai tout de même appris beaucoup de choses. Ce stage a donc été je pense bénéfique pour moi. Je n’avais pas vraiment d’idée sur ce que je voudrais faire plus tard, et je n’en ai toujours pas. 
