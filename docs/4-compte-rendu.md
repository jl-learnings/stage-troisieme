# Compte-rendu du stage

Tout ce que je vais écrire dans ce compte rendu est ce que j’ai compris ou retenu. Il est possible d’y trouver des erreurs.

## Lundi

### Présentation audit de qualité de code

Lundi matin, une personne de l’entreprise s’entraînait pour une présentation sur l’audit de qualité de code et ses collègues (dont mon maître de stage) le corrigeaient sur certains points ou lui proposaient des améliorations. J’ai donc assisté à cet entraînement.

L’audit de qualité de code sert à empêcher des erreurs dans la programmation en les montrant, comme quand on fait une faute d’orthographe en tapant du texte à l’ordinateur. C’est donc très utile en programmation.

### Présentation du métier de développeur

Lundi après-midi, une développeuse d’applications mobiles m’a présenté son métier. Elle m’a expliqué le fonctionnement d’un logiciel de programmation nommé GitLab, comment le système de coopération fonctionnait.

Elle m’a montré des lignes de code, m’a décrit la mise au point d’une application mobile, m’a dit qui travaillait avec son équipe pour mettre au point le design, le décor, la forme de l’application.

## Mardi

### Présentation du métier de codeur de serveurs vocaux interactifs

Mardi après-midi, c’est  un codeur  de serveurs vocaux  interactifs qui me présentait son métier.

Quand notre télévision se casse ou que notre box s’arrête de fonctionner, nous appelons des services  de réparation ou un service d'assistance. A ce moment-là, ce n’est pas un humain qui nous répond, mais un robot. Il nous demande alors de taper 1, 2, 3, ou encore un autre chiffre, selon le problème qu’on a.

Les codeurs de serveurs vocaux interactifs sont les personnes qui codent ces robots.

Il m’a expliqué comment fonctionnait  l’organisation de son équipe, mais aussi ce qu’était le  cloud et son fonctionnement, dont  il se  servait  beaucoup  pour  mettre  au point les  serveurs vocaux.

### Présentation sur le cloud

Mardi, plus tard dans l’après-midi, mon oncle m’a fait une présentation pour que je comprenne ce qu’était le cloud.

Il m’a expliqué ce qui se passait quand, par exemple, j’utilise Gmail. L’information que j’envoie passe d’abord par ma box (en l’occurrence la Freebox) avant d’entrer dans l’internet de France. De là, elle passe par l’internet mondial pour aller dans celui des Etats-Unis, où se trouvent les serveurs Gmail. Une fois arrivée chez Gmail, un système d’aiguillage (comme avec les trains quand il y a deux directions possibles à prendre) l’envoie vers un serveur libre, avant de refaire le chemin inverse vers moi.

![Orange!](img/internet.png)

Il m’a ensuite expliqué le fonctionnement du cloud. Le cloud est en fait constitué de plusieurs ordinateurs très puissants entreposés dans un entrepôt quelque part aux Etats-Unis. Ces ordinateurs sont divisés en plusieurs machines virtuelles, chacune se comportant comme un petit ordinateur indépendant.

## Mercredi

### Présentation sur la communication interne de l’entreprise

Mercredi matin, une personne m’a présenté ce qu’était la communication interne d’une entreprise, surtout celle d’Orange.

Le groupe OLS (Orange Labs Services) dans laquelle j’ai effectué mon stage possède des sites partout en France et même dans le monde.

Chez  Orange, beaucoup de moyens sont utilisés pour communiquer.Par exemple, il y a l’intranet, pour transmettre des objectifs, des actualités, des événements qui se passent au sein de l’entreprise.

Le métier de la personne qui m’a expliqué ça consiste pour une part  en rédiger des articles sur ce site. Ces articles sont rédigés en français et en anglais pour que les salariés travaillant en Egypte ou en Chine par exemple puissent les comprendre et donc se tenir au courant de ce qui se passe dans l’entreprise. Cet intranet est bien évidemment spécifique à Orange et accessible uniquement par les membres de l’entreprise.

Mais il n’y a pas que l’intranet pour communiquer au sein de l’entreprise. lls utilisent aussi un réseau social du nom de Plazza, lui aussi  spécifique à l’entreprise et accessible uniquement par ses membres.

Elle m’a expliqué que quand elle écrivait un article, il fallait qu’il soit court, sinon il ne donnerait pas envie d’être lu. Pour ça, elle peut aussi le mettre sous forme de powerpoint  ou  de vidéo. Dans ce cas, elle doit respecter une charte qui dit que certaines couleurs sont interdites, d’autres sont obligatoires, avec un minimum de pourcentage d’affichage, certaines ne doivent pas dépasser un quota de ce pourcentage (les couleurs autorisées sont le noir, le blanc, l’orange, le gris, le bleu marine, le rose, le violet et le jaune).

La charte dit aussi que certaines musiques ne doivent pas être utilisées dans les vidéos. Même  chose  pour  les illustrations. Cette charte est aussi valable pour les publicités. Toutes ces obligations et restrictions sont faites pour respecter l’esprit  d’Orange.

Elle m’a aussi parlé des événements qu’il pouvait y avoir au sein de l’entreprise, de comment son équipe et elle font pour les organiser et du temps que ça prenait.

### Présentation sur l’intelligence artificielle

Mercredi après-midi, une personne m’a expliqué comment fonctionne l’intelligence artificielle.

L’intelligence artificielle n’est pas vraiment intelligente. Elle ne réfléchit pas, contrairement à l’homme. Elle se sert de ce qu’on lui a appris ou de ce qu’elle a appris elle-même. Pour jouer à un jeu-vidéo “simple”, comme Pacman, l’intelligence artificielle “s’entraîne” d’abord, c’est à dire qu’elle essaie plein de possibilités et de combinaisons. Selon si la combinaison est bonne ou mauvaise, elle reçoit des points ou non, comme un chien qu’on dresserait en lui donnant  une croquette ou une réprimande selon s’il fait une bonne action ou non.

La personne qui m’a expliqué cela m’a montré un exemple d’une IA qui ne s’était que très peu entraîné à un jeu (1 heure). Elle était très mauvaise. Il m’a ensuite montré une IA qui s’était plus entraîné (3 jours), et elle était nettement meilleure.

L’IA peut aussi s’entraîner à reconnaître par exemple une image. Il l’avait entraînée à reconnaître des chiffres.

Il lui a demandé ensuite de nous dire quels chiffres étaient écrits sur une plaquette avec des chiffres écrits à la main et pour la plupart étrangement formés. Si on lui montrait un 3, il analysait la forme du chiffre et répondait qu’il y avait  (pour exemple) 92 % de chances que ce soit un 3. Il répondait en donnant des probabilités.

Dans les jeux-vidéo, on ne devrait pas parler d’intelligence artificielle car ce n’est pas quelque chose qui calcule, qui s’adapte en fonction de ce qu’elle sait. C’est en réalité une suite de programmes et d’algorithmes qui exécute ces programmes.

## Jeudi

### Présentation sur le Big Data

Jeudi matin, une personne m’a expliqué ce qu’est le Big Data. Quand on se sert d’Internet, notre box créé des fichiers contenant les données qu’on a utilisées. Ces données, par le biais d’internet, vont dans de gros serveurs appelés Big Data. Si jamais un des ordinateurs constituant les serveurs casse, les données qu’il contenait ne seront pas perdues pour autant, car elles ont été enregistrées dans tous les ordinateurs. Le cassé sera donc changé et téléchargera toutes les données qu’il lui faut.

Quand Orange constate un problème de connexion chez un nombre élevé de clients, elle se sert alors du Big Data pour voir si ce ne serait pas lié à des données qui auraient fait planter la connexion des utilisateurs. Les gens dont le métier est de rechercher ces problèmes s’appellent des data scientistes.

C’est aussi du Big Data dont se sert Google pour personnaliser les publicités qui s’afficheront sur votre téléphone ou ordinateur selon vos récentes recherches.

### Présentation Djingo

Jeudi après-midi, deux personnes m’ont présenté Djingo, l’assistant vocal à intelligence artificielle d’Orange. Elles m’ont expliqué en quoi consiste leur métier et comment fonctionne Djingo.

Quand on pose une question à Djingo, il se passe plusieurs étapes avant la réponse. La première est de capter l’onde sonore qui part de notre bouche. La seconde est de la reconnaître, de l’analyser et de la retranscrire en code.

Par exemple, si quelqu’un demande : “Quel temps fait-il aujourd’hui ?”, il va devoir comprendre qu’on lui demande la météo. Il lui faudra ensuite aller chercher l’information dont il a besoin. Pour dire la météo, il ira la chercher sur le site de Météo France, un partenaire d’Orange (car il ne peut pas prédire la météo tout seul).

Pour dire l’heure, il ira la chercher dans sa base de données (car il n’a pas besoin d’un site autre pour connaître l’heure). La quatrième étape consiste à transcrire le code pour pouvoir dire l’information. Tout ça se passe très rapidement.

Pour tester si Djingo est opérationnel, les développeurs font appel à une équipe qui se met à la place de l’utilisateur pour tester toutes les fonctionnalités et vérifier que tout fonctionne bien.

## Vendredi

### Présentation sur la communication pour un projet

Vendredi après-midi, une personne m’a expliqué comment il faisait pour communiquer avec son équipe sur un projet. Il m’a d’abord dit que pour fabriquer un téléphone, il faut environ 100 000 personnes. En effet, il faut fabriquer les matériaux le composant, les assembler, coder le téléphone…

Il m’a ensuite montré comment il communique sur un projet de groupe. Ils utilisent les méthodes agiles, c’est-à-dire qu’ils écrivent sur un tableau toutes les tâches à faire et se les répartissent en priorisant les plus importantes. Cela facilite le travail collaboratif. Aujourd’hui, ces tableaux sont virtuels. Ils sont appelés « bac logs ».

Un backlog est constitué de plusieurs parties. Les « features » qui sont les projets (exemple : passer un appel), les éléments de bac logs aussi appelés « stories », qui sont les différentes parties des features (exemple : pouvoir décrocher quand le téléphone sonne). Il y a ensuite les tâches, là où tout est répartit. Ensuite les tests, pour vérifier que tout fonctionne comme prévu. Et pour finir les « commits », pour approuver et enregistrer le travail.

Les membres de l’équipe ont souvent un temps limité pour terminer le projet, généralement un mois. Si jamais ils savent qu’ils ne vont pas le terminer à temps, ils priorisent les éléments les plus importants et se renseignent lorsqu’ils ne savent pas faire certaines choses.

Pour faire tout ça, différents moyens de communications sont utilisés. Dans le cas d’Orange, il y a Mattermost, un site sur lequel ils peuvent échanger, le mail, les réunions, qui permettent de faciliter le travail en commun et d’établir un sentiment de confiance entre les membres du projet. Il y a aussi les appels téléphoniques, et pour finir la messagerie instantanée comme Skype.
